#!/usr/bin/env node

/**
 * Created by USER on 23.03.2017.
 * @author Holly Schinsky
 * @see http://devgirl.org/2013/11/12/three-hooks-your-cordovaphonegap-project-needs/
 */

//this hook installs all your plugins

// add your plugins to this list--either
// the identifier, the filesystem location
// or the URL
var pluginlist = [
    'cordova-plugin-calendar',
    'cordova-plugin-compat',
    'cordova-plugin-crosswalk-webview',
    'cordova-plugin-file',
    'cordova-plugin-file-transfer',
    'cordova-plugin-globalization',
    'cordova-plugin-inappbrowser',
    'cordova-plugin-spinner-dialog',
    'cordova-plugin-whitelist',
    'cordova-plugin-zip'//,
];

// no need to configure below

var fs = require('fs');
var path = require('path');
var sys = require('sys');
var exec = require('child_process').exec;

function puts(error, stdout, stderr) {
    sys.puts(stdout)
}

pluginlist.forEach(function(plug) {
    exec("cordova plugin add " + plug, puts);
});
