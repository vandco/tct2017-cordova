#!/usr/bin/env node

/**
 * Created by USER on 23.03.2017.
 */

//
// This hook deletes various source files
// into the appropriate platform specific location
//


// configure all the files to delete

var filesToDelete = [
    "platforms/android/assets/www/lib/framework7/js/framework7.js",
    "platforms/android/assets/www/lib/framework7/js/framework7.js.map",
    "platforms/android/assets/www/lib/framework7/js/framework7.min.js.map",
    "platforms/android/assets/www/lib/framework7/css/framework7.ios.colors.css",
    "platforms/android/assets/www/lib/framework7/css/framework7.ios.css",
    "platforms/android/assets/www/lib/framework7/css/framework7.ios.rtl.css",
    "platforms/android/assets/www/lib/framework7/css/framework7.material.colors.css",
    "platforms/android/assets/www/lib/framework7/css/framework7.material.css",
    "platforms/android/assets/www/lib/framework7/css/framework7.material.rtl.css",
    "platforms/android/assets/www/lib/leaflet/leaflet-src.js",
    "platforms/android/assets/www/lib/leaflet/leaflet-src.map",

    "platforms/ios/www/lib/framework7/js/framework7.js",
    "platforms/ios/www/lib/framework7/js/framework7.js.map",
    "platforms/ios/www/lib/framework7/js/framework7.min.js.map",
    "platforms/ios/www/lib/framework7/css/framework7.ios.colors.css",
    "platforms/ios/www/lib/framework7/css/framework7.ios.css",
    "platforms/ios/www/lib/framework7/css/framework7.ios.rtl.css",
    "platforms/ios/www/lib/framework7/css/framework7.material.colors.css",
    "platforms/ios/www/lib/framework7/css/framework7.material.css",
    "platforms/ios/www/lib/framework7/css/framework7.material.rtl.css",
    "platforms/ios/www/lib/leaflet/leaflet-src.js",
    "platforms/ios/www/lib/leaflet/leaflet-src.map"
];

var fs = require('fs');
var path = require('path');

// no need to configure below
var rootDir = process.argv[2];

filesToDelete.forEach(function(obj) {
    var srcFile = path.join(rootDir, obj);
    console.log("deleting "+srcFile);
    if (fs.existsSync(srcFile)) {
        fs.unlinkSync(srcFile);
    } else {
        console.error('failed: no srcFile!');
    }
});
