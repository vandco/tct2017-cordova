; // intentionally left here
(function() {
    var AskController = Object.create(BaseController);
    Object.defineProperty(AskController, '_pageName', {value: 'ask'});
    Object.defineProperty(AskController, '_speaker', {writable: true});
    Object.defineProperty(AskController, 'onInit', {value: function ($pageContainer, page) {
        this._speaker = Schedule.speakers[page.query.speakerID];

        var photoSrc = '';
        if (typeof this._speaker.photoName !== typeof undefined) {
            photoSrc = cordova.file.cacheDirectory + 'unpacked/' + this._speaker.photoName;
        } else {
            if (this._speaker.sex === 'female') {
                photoSrc = 'img/female_100.png';
            } else {
                photoSrc = 'img/male_100.png';
            }
        }

        var $contentBlock = $pageContainer.find('.content-block');
        $contentBlock.find('.speaker-name').html(this._speaker['name_' + language]);

        var $img = $pageContainer.find('img');
        var $fromName = $pageContainer.find('[name="fromName"]');
        var $fromEmail = $pageContainer.find('[name="fromEmail"]');
        var $textArea = $pageContainer.find('textarea');
        var $btnSubmit = $contentBlock.find('button[type="submit"]');

        $img.attr('src', photoSrc);

        this.on('click', 'button[type="submit"]', (function(e) {
            e.preventDefault();

            if (
                !$fromName.val().trim().length
                || !$fromEmail.val().trim().length
                || !$textArea.val().trim().length
            ) {
                myApp.alert(_('all fields required'));
                return;
            }

            var $preloader = $$('<span />');
            $preloader.addClass('preloader').css('width', '50px').css('height', '50px').html($$('#tpl-preloader').html());
            $preloader.insertAfter($btnSubmit);
            $btnSubmit.hide();
            window.plugins.spinnerDialog.show(_('sending question'), _('please wait'), true);

            $$.ajax({
                url: 'http://schedule.endovascular.ru/schedule/sendQuestionToSpeaker/O8zd5znVDUniZuo8FRP2/' + language + '/' + this._speaker.id,
                crossDomain: true,
                dataType: 'json',
                timeout: 5000,
                method: 'POST',
                data: {
                    fromName: $fromName.val(),
                    fromEmail: $fromEmail.val(),
                    question: $textArea.val()
                },
                success: function(data) {
                    console.log(data);
                    myApp.alert(_('asked ok'));
                },
                error: function (xhr, response, status) {
                    console.log(xhr);
                    console.log(response);
                    console.log(status);
                    myApp.alert(_('asked error'));
                },
                complete: function () {
                    $preloader.remove();
                    $btnSubmit.show();
                    window.plugins.spinnerDialog.hide();
                }
            });

        }).bind(this));
    }});
    Object.defineProperty(AskController, 'onAfterAnimation', {value: function (page) {
        var $navbarRight = app.$getNavbar($$('#' + this._pageID)).find('.right');
        var $btnBack = $$('<a href="#" class="link back" />');
        $btnBack.html(_('back')).appendTo($navbarRight);
        myApp.sizeNavbars('.view-main');

        this.onBackButton = function() {
            $btnBack.click();
            return false;
        };
    }});


    window.AskController = function () {
        return AskController._construct();
    };
})();
