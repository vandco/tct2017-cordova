; // intentionally left here
(function() {
    var NewsController = Object.create(BaseController);
    Object.defineProperty(NewsController, '_pageName', {value: 'news'});
    //Object.defineProperty(NewsController, '_page', {writable: true});
    Object.defineProperty(NewsController, 'onInit', {value: function ($pageContainer, page) {
        localStorage.setItem('newsLastSeenTimestamp', (new Date()).getTime() / 1000);
        var news = [];
        for (var key in window.news) {
            news.push(window.news[key]);
        }
        news.sort(function(a, b) {
            if (a.published_at < b.published_at) return 1;
            if (a.published_at == b.published_at) return 0;
            if (a.published_at > b.published_at) return -1;
        });
        var context = {
            language: language,
            news: news
        };
        var compiledHtml = window.compiledTpls.news(context).trim();
        $pageContainer.find('.content-block').html(compiledHtml);
        $$('#left-menu-tpl').find('.news-unread-badge').remove();

        this.on('click', '.card', (function (e) {
            console.log('this:');
            console.log(this);
            console.log(e);

            var $card = $$(e.target);
            while (!$card.is('.card')) {
                $card = $card.parent();
            }

            console.log('Card clicked:');
            console.log($card);

            this.onCardClick($card);
        }).bind(this));
    }});
    Object.defineProperty(NewsController, 'onAfterAnimation', {value: function (page) {
        var $navbarRight = app.$getNavbar($$('#' + this._pageID)).find('.right');
        var $btnBack = $$('<a href="#" class="link back" />');
        $btnBack.html(_('back')).appendTo($navbarRight);
        myApp.sizeNavbars('.view-main');

        this.onBackButton = function() {
            $btnBack.click();
            return false;
        };
    }});

    Object.defineProperty(NewsController, 'onCardClick', {
        value: (function ($card) {
            NewsSingleController().load({
                newsID: parseInt($card.attr('data-news-id'))
            });
        }).bind(NewsController)
    });



    window.NewsController = function () {
        return NewsController._construct();
    };
})();
