; // intentionally left here
(function() {
    var SettingsController = Object.create(BaseController);
    Object.defineProperty(SettingsController, '_pageName', {value: 'settings'});
    Object.defineProperty(SettingsController, 'onInit', {value: function ($pageContainer, page) {
        var thisPtr = this;

        // по клику меняем css файл, меняя ios и material стиль. Выбор запоминаем в localStorage, чтобы при перезапуске приложения загружался нужный css
        this.on('change', 'select[name="style"]', function() {
            thisPtr.onStyleChange($$(this));
            return false;
        });

        // по клику меняем язык. Выбор запоминаем в localStorage, чтобы при перезапуске приложения загружался нужный язык
        this.on('change', 'select[name="language"]', function() {
            thisPtr.onLanguageChange($$(this));
            return false;
        });

        // тестовый режим :)
        if (!isGod) {
            var tapsToBecomeGod = 7;
            this.on('click', '#version-number', function () {
                tapsToBecomeGod--;
                if (!tapsToBecomeGod) {
                    tapsToBecomeGod = 7;
                    myApp.prompt('Введите пароль для перехода в тестовый режим:', 'Тестовый режим', function (password) {
                        if (password == 'iddqd') {
                            localStorage.setItem('god', 'true');
                            window.plugins.spinnerDialog.show('Переход в тестовый режим...', _('please wait'), true);
                            app.restart();
                        } else {
                            myApp.addNotification({
                                message: 'Неправильный пароль',
                                hold: 3000,
                                closeOnClick: true
                            });
                        }
                    })
                } else if (tapsToBecomeGod < 4) {
                    myApp.addNotification({
                        title: 'Тестовый режим',
                        message: 'Ещё ' + tapsToBecomeGod,
                        hold: 1000,
                        button: {
                            text: _('close')
                        }
                    });
                }
            });
        } else {
            this.on('click', '#abortGod', function () {
                myApp.confirm('Вам потребуется заново ввести пароль, если пожелаете вернуться в тестовый режим. <br /><br /> Вы уверены, что хотите выйти из тестового режима?', 'Стать простым смертным', function () {
                    localStorage.setItem('god', 'false');
                    window.plugins.spinnerDialog.show('Выход из тестового режима...', _('please wait'), true);
                    app.restart();
                })
            });
        }
    }});

    Object.defineProperty(SettingsController, 'onStyleChange', {value: function ($select) {
        localStorage.setItem("css", $select.val());
        setTimeout(function () {
            window.plugins.spinnerDialog.show(_('skin change in progress'), _('please wait'), true);
            app.restart();
        }, 1000);
    }});
    
    Object.defineProperty(SettingsController, 'onLanguageChange', {value: function ($select) {
        localStorage.setItem("language", $select.val());
        html10n.localize($select.val());
        html10n.unbind('localized');
        html10n.bind('localized', function() {
            setTimeout(function () {
                window.plugins.spinnerDialog.show(_('language change in progress'), _('please wait'), true);
                app.restart();
            }, 1000);
        });
    }});
    
    window.SettingsController = function () {
        return SettingsController._construct();
    };
})();
